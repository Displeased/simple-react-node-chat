var ChatApp = React.createClass({
    getInitialState: function () {
        return {
            messages: [],
            socket: window.io('http://localhost:3000'),
            user: undefined
        }
    },
    componentDidMount: function () {
        var self = this;
        this.state.socket.on("receive-message", function (msg) {
            var messages = self.state.messages;
            messages.push(msg);
            console.log(msg);
            self.setState({messages: messages});
            console.log(self.state.messages);
        });
    },
    submitMessage: function () {
        var body = document.getElementById('message').value;
        var message = {
            body: body,
            user: this.state.user || 'guest'
        };
        this.state.socket.emit("new-message", message);
        console.log(message);
    },

    pickUser: function () {
        var user = document.getElementById('user').value;
        this.setState({user: user});
    },

    render: function () {
        var self = this;
        var messages = self.state.messages.map(function (msg) {
            return <li className="right clearfix">
                <span className="chat-img pull-right">
                <img src="http://placehold.it/50/FA6F57/fff&text=" alt="User Avatar" className="img-circle"/>
                </span>
                <div className="chat-body clearfix">
                    <div className="header">
                        <strong className="pull-right primary-font">{msg.user}</strong>
                    </div>
                    <p>
                        {msg.body}
                    </p>
                </div>
            </li>
        });

        return (
            <div className="panel-body">
                <ul className="chat">
                    {messages}
                </ul>
                <input className="form-control input-sm" id="message" type="text"/>
                <button className="btn btn-warning btn-sm pull-right" onClick={() => self.submitMessage()}>Send
                    message
                </button>
                <br/>
                <input className="form-control input-sm" id="user" type="text" placeholder="choose a username"/>
                <button className="btn btn-warning btn-sm pull-right" onClick={() => self.pickUser()}>Select user
                </button>
            </div>
        )
    }
});

ReactDOM.render(
    <ChatApp/>,
    document.getElementById('chat')
);